include(../config.pri)
BASEDIR = $$relativePath("..")

CONFIG *= PKGCONFIG

PKGCONFIG *= glfw3

LIBS += -lvulkan

TARGET = imgui
TEMPLATE = lib

INCLUDEPATH += $${BASEDIR}/
DEPENDPATH += $${BASEDIR}/

SOURCES += *.cpp
HEADERS += *.h
